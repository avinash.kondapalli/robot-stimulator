import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PositionTest {
    @Test
    public void advanceX_whenXis7Yis3andDirectionIsE_shouldReturnXis8andYis3()
    {
        Position position =new Position(7,3);

        Position expectedOutput=new Position(8,3);
        Position actualOutput= position.advanceX(1);
        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void advanceY_whenXis7Yis3andDirectionIsN_shouldReturnXis7andYis4()
    {
        Position position =new Position(7,3);

        Position expectedOutput=new Position(7,4);
        Position actualOutput= position.advanceY(1);
        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void advanceX_whenXis7Yis3andDirectionIsW_shouldReturnXis6andYis3()
    {
        Position position =new Position(7,3);

        Position expectedOutput=new Position(6,3);
        Position actualOutput= position.advanceX(-1);
        assertEquals(actualOutput,expectedOutput);
    }

    @Test
    public void advanceY_whenXis7Yis3andDirectionIsS_shouldReturnXis7andYis2()
    {
        Position position =new Position(7,3);

        Position expectedOutput=new Position(7,2);
        Position actualOutput= position.advanceY(-1);
        assertEquals(actualOutput,expectedOutput);
    }

}
