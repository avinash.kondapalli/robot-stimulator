import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RobotTest {
    @Test
    public void move_whenXis7yIs3directionIsNorthAndMovedtoRight_ResultIsXis7Yis3DirectionIsEast() {
        Position position = new Position(7, 3);
        String instruction = "R";
        Location location = new Location(position, Direction.NORTH);
        Robot robot = new Robot(location);

        Location actualOutput = new Location(position, Direction.EAST);
        Location expectedOutput = robot.move(instruction);

        assertEquals(expectedOutput, actualOutput);

    }

    @Test
    public void move_whenXis7yIs3directionIsNorthAndMovedToLeft_ResultIsXis7Yis3DirectionIsWest() {
        Position position = new Position(7, 3);
        String instruction = "L";
        Location location = new Location(position, Direction.NORTH);
        Robot robot = new Robot(location);

        Location actualOutput = new Location(position, Direction.WEST);
        Location expectedOutput = robot.move(instruction);

        assertEquals(expectedOutput, actualOutput);

    }

    @Test
    public void move_whenXis7yIs3directionIsNorthAndMovedByAdvance_ResultIsXis7Yis3DirectionIsEast() {
        Position position = new Position(7, 3);
        String instruction = "A";
        Location location = new Location(position, Direction.NORTH);
        Robot robot = new Robot(location);

        Location actualOutput = new Location(new Position(7, 4), Direction.NORTH);
        Location expectedOutput = robot.move(instruction);

        assertEquals(expectedOutput, actualOutput);

    }

    @Test
    public void move_whenXis7yIs3directionIsNorthAndMovedByRAALAL_ResultIsXis9Yis4DirectionIsWest() {
        Position position = new Position(7, 3);
        String instruction = "RAALAL";
        Location location = new Location(position, Direction.NORTH);
        Robot robot = new Robot(location);

        Location actualOutput = new Location(new Position(9, 4), Direction.WEST);
        Location expectedOutput = robot.move(instruction);

        assertEquals(expectedOutput, actualOutput);

    }

    @Test(expected = InvalidDirectionException.class)
    public void move_whenXis7yIs3directionIsNorthAndMovedInDirectionXYZ_ResultIsInvalidDirectionException() {
        Position position = new Position(7, 3);
        String instruction = "XYZ";
        Location location = new Location(position, Direction.NORTH);
        Robot robot = new Robot(location);

        Location actualOutput = new Location(new Position(0, 0), Direction.NORTH);
        Location expectedOutput = robot.move(instruction);

        assertEquals(expectedOutput, actualOutput);
    }



}
