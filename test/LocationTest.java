import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LocationTest {
    @Test
    public void advance_whenXis7andYis3andDirectionIsEast_ShouldReturnXis8Yis3DirectionEast()
    {
        Position position=new Position(7,3);
        Direction direction=Direction.EAST;
        Location location=new Location(position,direction);
        Location expectedOutput=new Location(new Position(8,3),Direction.EAST);
        Location actualOutput=location.advance();
        assertEquals(expectedOutput,actualOutput);
    }
    @Test
    public void advance_whenXis7andYis3andDirectionIsWest_ShouldReturnXis8Yis3DirectionWest()
    {
        Position position=new Position(7,3);
        Direction direction=Direction.WEST;
        Location location=new Location(position,direction);
        Location expectedOutput=new Location(new Position(6,3),Direction.WEST);
        Location actualOutput=location.advance();
        assertEquals(expectedOutput,actualOutput);
    }
    @Test
    public void advance_whenXis7andYis3andDirectionIsNorth_ShouldReturnXis8Yis3DirectionNorth()
    {
        Position position=new Position(7,3);
        Direction direction=Direction.NORTH;
        Location location=new Location(position,direction);
        Location expectedOutput=new Location(new Position(7,4),Direction.NORTH);
        Location actualOutput=location.advance();
        assertEquals(expectedOutput,actualOutput);
    }
    @Test
    public void advance_whenXis7andYis3andDirectionIsSouth_ShouldReturnXis8Yis3DirectionSouth()
    {
        Position position=new Position(7,3);
        Direction direction=Direction.SOUTH;
        Location location=new Location(position,direction);
        Location expectedOutput=new Location(new Position(7,2),Direction.SOUTH);
        Location actualOutput=location.advance();
        assertEquals(expectedOutput,actualOutput);
    }


}
