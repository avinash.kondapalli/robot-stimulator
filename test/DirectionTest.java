import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class DirectionTest {
    @Test
    public void dotRight_whenWeAccessEastDotRight_shouldReturnSouth(){
        Direction direction=Direction.EAST;

        Direction actualOutput=Direction.SOUTH;
        Direction expectedOutput=direction.right;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotLeft_whenWeAccessEastDotLeft_shouldReturnNorth(){
        Direction direction=Direction.EAST;

        Direction actualOutput=Direction.NORTH;
        Direction expectedOutput=direction.left;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotRight_whenWeAccessNorthDotRight_shouldReturnEastt(){
        Direction direction=Direction.NORTH;

        Direction actualOutput=Direction.EAST;
        Direction expectedOutput=direction.right;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotRight_whenWeAccessNorthDotLeft_shouldReturnWest(){
        Direction direction=Direction.NORTH;

        Direction actualOutput=Direction.WEST;
        Direction expectedOutput=direction.left;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotRight_whenWeAccessSouthDotRight_shouldReturnWest(){
        Direction direction=Direction.SOUTH;

        Direction actualOutput=Direction.WEST;
        Direction expectedOutput=direction.right;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotLeft_whenWeAccessSouthDotLeft_shouldReturnEast(){
        Direction direction=Direction.SOUTH;

        Direction actualOutput=Direction.EAST;
        Direction expectedOutput=direction.left;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotRight_whenWeAccessWestDotRight_shouldReturnNorth(){
        Direction direction=Direction.WEST;

        Direction actualOutput=Direction.NORTH;
        Direction expectedOutput=direction.right;

        assertEquals(actualOutput,expectedOutput);
    }
    @Test
    public void dotLeft_whenWeAccessWestDotLeft_shouldReturnSouth(){
        Direction direction=Direction.WEST;

        Direction actualOutput=Direction.SOUTH;
        Direction expectedOutput=direction.left;

        assertEquals(actualOutput,expectedOutput);
    }
}
