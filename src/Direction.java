public enum Direction {
    EAST,
    NORTH,
    SOUTH,
    WEST;


    public Direction right;
    public Direction left;

    static {
        EAST.right = SOUTH;
        EAST.left = NORTH;
        NORTH.right = EAST;
        NORTH.left = WEST;
        WEST.right = NORTH;
        WEST.left = SOUTH;
        SOUTH.right = WEST;
        SOUTH.left = EAST;
    }


    public Direction moveAccordingly(char instruction) {
        if (instruction == 'R') {
            return this.right;
        }
        return this.left;

    }
}
