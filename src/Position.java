import java.util.Objects;

public class Position {
    protected int xCoordinate;
    protected int yCoordinate;


    Position(int xCoordinate, int yCoordinate) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    public Position advanceX(int i) {
        return (new Position(xCoordinate + i, yCoordinate));
    }


    public Position advanceY(int i) {
        return (new Position(xCoordinate, yCoordinate + i));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position that = (Position) o;
        return xCoordinate == that.xCoordinate &&
                yCoordinate == that.yCoordinate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xCoordinate, yCoordinate);
    }

}
