public class Robot {
    protected Location location;

    public Robot(Location location) {
        this.location = location;
    }


    public Location move(String instruction) {

        for (int i = 0; i < instruction.length(); i++) {

            if (!(instruction.charAt(i) == 'R' || instruction.charAt(i) == 'L' || instruction.charAt(i) == 'A')) {
                throw new InvalidDirectionException();
            }
            if (instruction.charAt(i) == 'A') {
                location = location.advance();
                continue;
            }

            location.direction = location.direction.moveAccordingly(instruction.charAt(i));
        }

        return location;
    }


}
